Install python requirements:

```
poetry install
```

Install tailwind:

```
yarn
```

Run tailwind:

```
yarn css
```

The tailwind config will look in any `py` or `html` file and compile `src/dist.css` against it.

Run Panel:

```
panel serve src/app.py --autoreload
```