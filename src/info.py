import panel as pn

pn.extension(sizing_mode="stretch_width")

info = pn.pane.HTML("""
<div class="py-4 text-center bg-indigo-900 lg:px-4">
  <div class="flex items-center p-2 leading-none text-indigo-100 bg-indigo-800 lg:rounded-full lg:inline-flex" role="alert">
    <span class="flex px-2 py-1 mr-3 text-xl font-bold uppercase bg-indigo-500 rounded-full">WOW</span>
    <span class="flex-auto mr-2 font-semibold text-left">Panel supports custom HTML components using Tailwind</span>
  </div>
</div>
""", height=100)
