import panel as pn
import param


class CustomButton(pn.reactive.ReactiveHTML):
    clicks = param.Integer()
        
    _template = """\
        <div id="pn-container" style="height:100%;width:100%">
        <button id="button" class="px-4 py-2 font-bold text-white bg-indigo-500 rounded hover:bg-indigo-700"
        onclick="${_button_click}"
        >{{ name }}</button></div>
    """


    def _button_click(self, event):
        self.clicks += 1

