import panel as pn

from src.button import CustomButton
from src.info import info

# ---------------------------------------------------------------------------- #
#                                 Panel Config                                 #
# ---------------------------------------------------------------------------- #
pn.config.css_files = ["src/styles/dist.css", "src/libs/notifications/notifications.css"]
pn.config.sizing_mode = "stretch_width"

# ---------------------------------------------------------------------------- #
#                                   Panel App                                  #
# ---------------------------------------------------------------------------- #
button1 = CustomButton(name="Click Me", height=38)

@pn.depends(clicks=button1.param.clicks)
def text(clicks=1):
    return f"I've been clicked {clicks} number of times"

pn.Column(
    info, button1, text
).servable()
